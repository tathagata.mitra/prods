const Joi = require('joi')
const { 
    handleProducts, updateCurrency
} = require('../models/api.model');

const { 
    validateProduct, validateProductID, validateProductandCurrencyID
} = require('../models/validation.model');

/**
 * Add a single product
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
const addProduct = async (req, res) => {

    // Validate the input parameters
    let status = validateProduct(req.body)

    // If input data contains validation error then output error message
    if(status.error) {
        // Return error details
        return res.status(status.code).send({status:'Error', message: status.message});
    }

    // Save product
    let {data, status_code, message} = await handleProducts(req.body, 'add');

    // Return details
    return res.status(status_code).send({status:status_code !==200?'Error':'OK', message, data});
}

/**
 * Get a single product
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
const getProduct = async (req, res) => {
   
    // Validate the input parameters
    let datastatus = validateProductandCurrencyID({
        id: req.params.id || '',
        currency: req.query.currency || ''
    })

    // If input data contains validation error then output error message
    if(datastatus.error) 
        return res.status(datastatus.code).send({status:'Error', message: datastatus.message});

    // Get products data
    const {data, status_code, message} = await handleProducts(req.params.id, 'get', (req.query.currency || ''))

    // Return details
    return res.status(status_code).send({status:status_code !==200?'Error':'OK', message, data});
}

/**
 * Get most viewed products
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
const getMostViewed = async (req, res) => {

    // Get most viewed products
    const {data, status_code, message} = await handleProducts(0, 'mostviewed')

    // Return details
    return res.status(status_code).send({status:status_code !==200?'Error':'OK', message, data});
}


/**
 * Delete product
 * @param {*} req
 * @param {*} res
 * @returns 
 */
const deleteProduct = async (req, res) => {

    // Validate the input parameters
    let status = validateProductID(req.params)

    // If input data contains validation error then output error message
    if(status.error) 
        return res.status(status.code).send({status:'Error', message: status.message});

    // Delete a products
    const {data, status_code, message} = await handleProducts(req.params.id, 'delete')

    // Return details
    return res.status(status_code).send({status:status_code !==200?'Error':'OK', message, data});
}

/**
 * Update currency
 * @param {*} res 
 * @returns 
 */
const updateCurrencyStatus = async (req, res) => {

    // Update status
    const {data, status_code, message} = await updateCurrency()

    // Return details
    return res.status(status_code).send({status:status_code !==200?'Error':'OK', message, data});
}

module.exports = { addProduct, getProduct, getMostViewed, deleteProduct, updateCurrencyStatus }