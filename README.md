# Product management

This API is for handling products.

## Local installation

1. Make sure you have npm and node installed, if not then its recommended using a Node version manager like [nvm](https://github.com/nvm-sh/nvm) to install Node.js and npm.


2. To download the latest version of npm, on the command line, run the following command:
    ```bash
    npm install -g npm
    ```

3. To see if you already have Node.js and npm installed and check the installed version, run the following commands:
    ```bash
    node -v
    npm -v
    ```

4. Now download the source code and locate the directory:
    ```bash
    cd /[DIRECTORY]/
    ```

5. Rename enviornment file: **.env.example** to **.env**


6. Download and install all npm local packages, run the below command:
    ```bash
    npm install --omit=dev
    ```

    OR

    ```bash
    npm i
    ```

7. Once installed rename .env.example into .env:
    ```bash
    node app
    ```

8. Now run node.js app:
    ```bash
    node app
    ```

9. Once node is running you can access the API using http://localhost:8888/ (follow the API documentation for more details)


## Setup AWS Lambda (Serverless) application

1. Make sure the IAM user have the following permissions:
    *  AmazonS3
    *  AmazonDynamoDB
    *  AWSCodeDeploy
    *  CloudWatchLogs
    *  AmazonAPIGateway
    *  AmazonRoute53
    *  AWSCertificateManager
    *  AWSCloudFOrmation
    *  AWSLambda<br /><br />
 
2. Open [Serverless App](https://app.serverless.com/) and login.
 
3. Click **org** from left menu bar and then select **providers** and add the same IAM users.

4. Once provider is added, select **apps** from left menu and then click on **create app** button on top right

5. Click on create **serverless framework**
 
6. Open **serverless.yml** and add **service** and **app** name (example: **service: xyzname** and **app: xyzname-app** and click **deploy**
 
7. Add the following code under **provider** section of **serverless.yml** which creates an **environment** variable with name of **ENV_NAME**
    ```
    provider:
    name: aws
    runtime: nodejs14.x
    environment:
        ENV_NAME: ${opt:stage, 'dev'}
    ```
 
8. Follow the instruction to login and deploy 
    *  **serverless login**
    *  **serverless** 
    *  **serverless deploy**<br /><br />
 
9. Deploy default stage using **serverless deploy**
 
10. Deploy only production or dev
    *  **serverless deploy --stage prod --region us-east-1** OR
    *  **serverless deploy --stage prod** OR
    *  **serverless deploy --stage dev**

## Reference
[Serverless](https://app.serverless.com/)