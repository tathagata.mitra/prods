const  express = require('express');
const router = express.Router();

// Default request
router.all("/", async(req, res)=>{
    return res.send({status: 'OK', message: "App is running....."});
});

module.exports = router;